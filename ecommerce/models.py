from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

from django.db.models.signals import pre_save, post_save
from ecommerce.utils import unique_slug_generator

from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.

class user_information(models.Model):
    name            = models.CharField(max_length=255,blank=True,null=True)
    email           = models.CharField(max_length=255,blank=True,null=True)
    phone           = models.CharField(max_length=255,blank=True,null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.email)

    class Meta:
        verbose_name_plural = "User information"

class nutritional_files(models.Model):
    file_name       = models.CharField(max_length=512,blank=True,null=True)
    file_link       = models.FileField(upload_to="product/nutritional-files/",blank=True,null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.file_link)

    class Meta:
        verbose_name_plural = "Nutrition File"


class user_nutrition(models.Model):
    name            = models.CharField(max_length=255,blank=True,null=True)
    email           = models.CharField(max_length=255,blank=True,null=True)
    file_downloaded = models.ForeignKey(nutritional_files, on_delete=models.CASCADE)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.email)
    class Meta:
        verbose_name_plural = "User details Nutrition Files"




class other_books(models.Model):
    file_name       = models.CharField(max_length=512,blank=True,null=True)
    file_image       = models.ImageField(upload_to="product/other-books-image/",blank=True,null=True)
    file_link       = models.FileField(upload_to="product/other-books-file/",blank=True,null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.file_name)

    class Meta:
        verbose_name_plural = "Other Books File"


class user_other_book_details(models.Model):
    first_name      = models.CharField(max_length=255,blank=True,null=True)
    last_name       = models.CharField(max_length=255,blank=True,null=True)
    email           = models.CharField(max_length=255,blank=True,null=True)
    phone_number    = models.CharField(max_length=255,blank=True,null=True)
    street_address  = models.CharField(max_length=255,blank=True,null=True)
    city            = models.CharField(max_length=255,blank=True,null=True)
    state           = models.CharField(max_length=255,blank=True,null=True)
    pincode         = models.CharField(max_length=255,blank=True,null=True)
    country         = models.CharField(max_length=255,blank=True,null=True)
    comments        = models.CharField(max_length=255,blank=True,null=True)
    file_downloaded = models.ForeignKey(other_books, on_delete=models.CASCADE, blank=True, null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.email)
    class Meta:
        verbose_name_plural = "User details Other books"


class extra_books(models.Model):
    file_name       = models.CharField(max_length=512,blank=True,null=True)
    file_image       = models.ImageField(upload_to="product/other-books-image/",blank=True,null=True)
    file_link       = models.FileField(upload_to="product/other-books-file/",blank=True,null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.file_name)

    class Meta:
        verbose_name_plural = "extra books"


class extra_book_details(models.Model):
    first_name      = models.CharField(max_length=255,blank=True,null=True)
    last_name       = models.CharField(max_length=255,blank=True,null=True)
    email           = models.CharField(max_length=255,blank=True,null=True)
    phone_number    = models.CharField(max_length=255,blank=True,null=True)
    street_address  = models.CharField(max_length=255,blank=True,null=True)
    city            = models.CharField(max_length=255,blank=True,null=True)
    state           = models.CharField(max_length=255,blank=True,null=True)
    pincode         = models.CharField(max_length=255,blank=True,null=True)
    country         = models.CharField(max_length=255,blank=True,null=True)
    comments        = models.CharField(max_length=255,blank=True,null=True)
    file_downloaded = models.ForeignKey(extra_books, on_delete=models.CASCADE, blank=True, null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.email)
    class Meta:
        verbose_name_plural = "extra book details"



class product_category(models.Model):
    name            = models.CharField(max_length=255,blank=True,null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name_plural = "Product category"

class test_links(models.Model):
    test_link       = models.CharField(max_length=500,blank=True,null=True)
    def __str__(self):
        return str(self.test_link)

class test_series_details(models.Model):
    name            = models.CharField(max_length=255,blank=True,null=True)
    image           = models.ImageField(upload_to="product/test-series/images/",blank=True,null=True)
    test_link       = models.ManyToManyField(test_links)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name_plural = "Exam/Test Details"

class keywords(models.Model):
    keyword       = models.CharField(max_length=100,blank=True,null=True)
    def __str__(self):
        return str(self.keyword)
    class Meta:
        verbose_name_plural = "Keywords"

class tags(models.Model):
    tag       = models.CharField(max_length=100,blank=True,null=True)
    def __str__(self):
        return str(self.tag)
    class Meta:
        verbose_name_plural = "Tags"


class product_table(models.Model):
    name            = models.CharField(max_length=255,blank=True,null=True)
    name_slug       = models.CharField(max_length=255,blank=True,null=True)
    image           = models.ImageField(upload_to="product/images/",blank=True,null=True)
    category        = models.ForeignKey(product_category, on_delete=models.CASCADE)
    price           = models.DecimalField(max_digits=6, decimal_places=2,default=0)
    author          = models.CharField(max_length=255,blank=True,null=True)
    publisher       = models.CharField(max_length=255,blank=True,null=True)
    isbn            = models.CharField(max_length=255,blank=True,null=True)
    description     = RichTextUploadingField(blank=True,null=True)
    meta_description = models.TextField(max_length=512,blank=True,null=True)
    keywords        = models.ManyToManyField(keywords)
    tags            = models.ManyToManyField(tags)
    min_buy_quantity = models.DecimalField(max_digits=3, decimal_places=0,default=0)
    max_buy_quantity = models.DecimalField(max_digits=3, decimal_places=0,default=0)

    digital_book    = models.FileField(upload_to="product/digital-books/",blank=True,null=True)
    test_series     = models.ForeignKey(test_series_details, on_delete=models.CASCADE,blank=True,null=True)
    is_book         = models.BooleanField(default=False)
    is_ebook        = models.BooleanField(default=False)
    is_test_series  = models.BooleanField(default=False)
    is_other_item   = models.BooleanField(default=False)

    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def get_absolute_url(self):
        return reverse("products:detail", kwargs={"slug": self.title_slug,"id":self.id})

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name_plural = "Product information"

def product_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.name_slug:
        instance.name_slug = unique_slug_generator(instance)

pre_save.connect(product_pre_save_receiver, sender=product_table)


class cart_table(models.Model):
    user                = models.ForeignKey(user_information, on_delete=models.CASCADE, null=True, blank=True)
    # order               = models.OneToOneField('order_table',on_delete=models.CASCADE, null=True, blank=True)
    total_price         = models.DecimalField(max_digits=6, decimal_places=2,default=0)
    grand_total_price   = models.DecimalField(max_digits=6, decimal_places=2,default=0)
    shipping_price      = models.DecimalField(max_digits=6, decimal_places=2,default=0)
    is_empty            = models.BooleanField(default=True)
    checkout            = models.BooleanField(default=False)
    timestamp           = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        if self.user:
            return str(self.user.email)+"_"+str(self.id)
        else:
            return "guest"+"_"+str(self.id)

    class Meta:
        verbose_name_plural = "Cart details"


class cart_items(models.Model):
    cart            = models.ForeignKey(cart_table, on_delete=models.CASCADE)
    item            = models.ForeignKey(product_table, on_delete=models.CASCADE)
    quantity        = models.IntegerField(default=0,blank=True,null=True)
    line_total      = models.DecimalField(max_digits=6, decimal_places=2,default=0)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.cart.id)

    class Meta:
        verbose_name_plural = "Cart items"



class shipping_information(models.Model):
    user            = models.ForeignKey(user_information,on_delete=models.CASCADE)
    street_address  = models.CharField(max_length=255,blank=True,null=True)
    pincode         = models.CharField(max_length=255,blank=True,null=True)
    city            = models.CharField(max_length=255,blank=True,null=True)
    state            = models.CharField(max_length=255,blank=True,null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.user.email)

    class Meta:
        verbose_name_plural = "Shipping information"

class billing_information(models.Model):
    user            = models.ForeignKey(user_information,on_delete=models.CASCADE)
    street_address  = models.CharField(max_length=255,blank=True,null=True)
    pincode         = models.CharField(max_length=255,blank=True,null=True)
    city            = models.CharField(max_length=255,blank=True,null=True)
    state           = models.CharField(max_length=255,blank=True,null=True)
    timestamp       = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.user.email)

    class Meta:
        verbose_name_plural = "Billing information"



class order_table(models.Model):
    user                         = models.ForeignKey(user_information, on_delete=models.CASCADE)
    cart                         = models.OneToOneField(cart_table,on_delete=models.CASCADE)
    shipping_address            = models.ForeignKey(shipping_information, on_delete=models.CASCADE)
    billing_information          = models.ForeignKey(billing_information, on_delete=models.CASCADE, null=True, blank=True)
    order_id                     = models.CharField(max_length=255,blank=True,null=True)
    stripe_payment_intent        = models.CharField(max_length=255,blank=True,null=True)
    has_paid                    = models.BooleanField(default=False)
    delivered                   = models.BooleanField(default=False)
    order_date                  = models.DateTimeField(blank=True,null=True)
    timestamp                   = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return str(self.user.email)

    class Meta:
        verbose_name_plural = "Order details"


class test_series_code(models.Model):
    test_series   = models.ForeignKey(test_series_details, on_delete=models.CASCADE,blank=True,null=True)
    test_code = models.CharField(max_length=255,blank=True,null=True)

    def __str__(self):
        return str(self.test_series)

    class Meta:
        verbose_name_plural = "Test Series Code"
