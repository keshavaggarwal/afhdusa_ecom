from django.conf import settings

from django.core.mail import send_mail
from django.template.loader import get_template
from ecommerce.models import order_table, cart_table

from celery import shared_task



@shared_task
def send_contact_form_email(context):

    txt_ = get_template("contactform/confirm.txt").render(context)
    html_ = get_template("contactform/confirm.html").render(context)
    subject = 'New Query from afhdusa.org - '+ context['email']
    from_email = settings.DEFAULT_FROM_EMAIL
    recipient_list = ['srao@afhdusa.org','srao123@gmail.com']
    sent_mail = send_mail(
                subject,
                txt_,
                from_email,
                recipient_list,
                html_message=html_,
                fail_silently=False,
        )

    return 'success'


@shared_task
def send_confirm_email(context):
    order_inst = order_table.objects.get(id=context['order_id'])

    user_email = context['email']
    mail_data = {
        "order_inst":order_inst,
        "email":user_email,
        "ebook_cart":context['ebook_cart'],
        "test_series_cart":context['test_series_cart'],
        "base_url":settings.BASE_URL,
        "shipping_price":settings.SHIPPING_PRICE
    }
    txt_ = get_template("payments/confirm/confirm.txt").render(mail_data)
    html_ = get_template("payments/confirm/confirm.html").render(mail_data)
    subject = 'Hi '+ user_email +' Your order with AFHD-USA is Successfull'
    from_email = settings.DEFAULT_FROM_EMAIL
    recipient_list = [user_email,'srao123@gmail.com']
    sent_mail = send_mail(
                subject,
                txt_,
                from_email,
                recipient_list,
                html_message=html_,
                fail_silently=False,
        )

    return 'success'



@shared_task
def send_code_email(context):
    txt_ = get_template("code/confirm.txt").render(context)
    html_ = get_template("code/confirm.html").render(context)
    subject = 'Code from afhdusa.org - '+ context['email']
    from_email = settings.DEFAULT_FROM_EMAIL
    recipient_list = [context['email']]
    sent_mail = send_mail(
                subject,
                txt_,
                from_email,
                recipient_list,
                html_message=html_,
                fail_silently=False,
        )

    return 'success'
