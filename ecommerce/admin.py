from django.contrib import admin
import nested_admin

from .models import (
                        user_information,
                        product_category,
                        product_table,
                        cart_table,
                        cart_items,
                        shipping_information,
                        order_table,
                        user_nutrition,
                        nutritional_files,
                        test_series_details,
                        test_links,
                        test_series_code,
                        keywords,
                        tags,
                        other_books,
                        user_other_book_details,
                        extra_books,
                        extra_book_details
                    )

# Register your models here.

admin.site.site_header = 'AFHD-USA'
admin.site.site_title = 'AFHD-USA'
admin.site.index_title = 'AFHD-USA Administration'


class user_information_Admin(admin.ModelAdmin):
    # display datetime in the changelist
    list_display = ('name','email','timestamp',)
    # display datetime when you edit comments
    readonly_fields = ('timestamp',)
    # optional, use only if you need custom ordering of the fields
    # fields = ('title', 'body', 'datetime')

admin.site.register(user_information,user_information_Admin)
admin.site.register(other_books)
admin.site.register(user_other_book_details)

admin.site.register(product_category)

admin.site.register(extra_books)

admin.site.register(extra_book_details)


class keywordInline(admin.TabularInline):
    model = product_table.keywords.through

class tagInline(admin.TabularInline):
    model = product_table.tags.through


class tagsAdmin(admin.ModelAdmin):
    inlines = [
        tagInline,
    ]

class keywordsAdmin(admin.ModelAdmin):
    inlines = [
        keywordInline,
    ]

class productAdmin(admin.ModelAdmin):
    list_display = ('name','price',)
    inlines = [
        tagInline,keywordInline
    ]
    exclude = ('keywords','tags')


admin.site.register(product_table,productAdmin)
admin.site.register(tags,tagsAdmin)
admin.site.register(keywords,keywordsAdmin)

admin.site.register(cart_items)
admin.site.register(shipping_information)
admin.site.register(user_nutrition)
admin.site.register(nutritional_files)
admin.site.register(test_series_code)


class itemsInline(nested_admin.NestedStackedInline):
    model = cart_items

class cartInline(nested_admin.NestedModelAdmin):
    inlines = [itemsInline]

admin.site.register(cart_table,cartInline)



class testInline(admin.TabularInline):
    model = test_series_details.test_link.through

class test_linksAdmin(admin.ModelAdmin):
    inlines = [
        testInline,
    ]

class test_series_detailsAdmin(admin.ModelAdmin):
    inlines = [
        testInline,
    ]
    exclude = ('test_link',)

admin.site.register(test_series_details,test_series_detailsAdmin)
admin.site.register(test_links,test_linksAdmin)

# class cartInline2(nested_admin.NestedStackedInline):
#     model = cart_table
#     inlines = [itemsInline]
#
#
# class OrderAdmin(nested_admin.NestedModelAdmin):
#     inlines = [cartInline2]
#     model = order_table
#


# admin.site.register(order_table,OrderAdmin)
admin.site.register(order_table)
