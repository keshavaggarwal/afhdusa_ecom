from django.shortcuts import render,redirect,get_object_or_404
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.db.models import Q
from django.conf import settings
from datetime import date, datetime
from django.contrib.auth import authenticate, login as auth_login,logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
import decimal

from .tasks import send_confirm_email, send_contact_form_email, send_code_email

from .models import (   user_information,
                        product_category,
                        product_table,
                        cart_table,
                        cart_items,
                        shipping_information,
                        billing_information,
                        order_table,
                        nutritional_files,
                        user_nutrition,
                        other_books,
                        user_other_book_details,
                        test_series_code,
                        extra_books,
                        extra_book_details
                    )

from random import randint, randrange

import stripe
# Create your views here.

stripe.api_key = settings.STRIPE_KEY

domain = settings.BASE_URL

shipping_price = settings.SHIPPING_PRICE


def check_ebook_testSeries_only(cart_inst):
    only_ebook_in_cart = True
    only_testSeries_in_cart = True
    if cart_inst:
        for itm in cart_inst.cart_items_set.all():
            if not itm.item.is_ebook:
                only_ebook_in_cart = False
            if not itm.item.is_test_series:
                only_testSeries_in_cart = False
        context = {
            "only_ebook":only_ebook_in_cart,
            "only_testSeries":only_testSeries_in_cart
        }
        return


def get_cart_instance(request):
    cart_id = request.session.get("cart_id", None)

    if cart_id:
        try:
            qs = cart_table.objects.get(id=cart_id)
            cart_instance = qs
            return cart_instance
        except:
            qs = cart_table.objects.create(user=None)
            cart_instance = qs
            request.session['cart_id'] = qs.id
            return cart_instance
    else:
        return None


def index(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'index.html',context)

def temples_list(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'temples-list.html',context)


def who_we_are(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'who-we-are.html',context)


def vision_mission(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'vision-mission.html',context)


def what_we_do(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'what-we-do.html',context)


def our_team(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'team.html',context)

def project_nutrition(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'project-nutrition.html',context)


def project_knowledge(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'project-knowledge.html',context)


def nutrition_food_file(request):
    cart_inst = get_cart_instance(request)
    nutritional_files_inst = nutritional_files.objects.all()
    context = {
        "cart_inst":cart_inst,
        "nutritional_files_inst":nutritional_files_inst
    }
    return render(request,'nutrition-file.html',context)


def nutritional_file_user(request):
    if request.method == "POST":
        name = request.POST.get("name")
        email = request.POST.get("email")
        file_id = request.POST.get("file_id")

        file_inst = nutritional_files.objects.get(id=file_id)
        user_inst = user_nutrition.objects.get_or_create(
                                        name=name,
                                        email=email,
                                        file_downloaded=file_inst
                                    )

        return JsonResponse({"success":1,"file_url":file_inst.file_link.url},safe=False)


def other_books_download_file(request):
    cart_inst = get_cart_instance(request)
    other_books_inst = other_books.objects.all()
    context = {
        "cart_inst":cart_inst,
        "other_books_inst":other_books_inst
    }
    return render(request,'other-books-download.html',context)


def other_free_books_user(request):
    if request.method == "POST":
        if not request.POST.get("code"):
            first_name = request.POST.get("first_name")
            last_name = request.POST.get("last_name")
            email = request.POST.get("email")
            phone_number = request.POST.get("phone_number")
            street_address = request.POST.get("street_address")
            city = request.POST.get("city")
            state = request.POST.get("state")
            pincode = request.POST.get("pincode")
            country = request.POST.get("country")
            comments = request.POST.get("comments")
            file_id = request.POST.get("file_id")

            # file_inst = other_books.objects.get(id=file_id)
            user_inst, created = user_other_book_details.objects.get_or_create(
                                            first_name=first_name,
                                            last_name=last_name,
                                            phone_number=phone_number,
                                            email=email,
                                            street_address=street_address,
                                            city=city,
                                            state=state,
                                            pincode=pincode,
                                            country=country,
                                            comments=comments
                                            # file_downloaded=file_inst
                                        )
            fcode = randint(1000,9999)
            context = {
                "code":fcode,
                "email":email,
                "first_name":first_name
            }
            request.session['f_code'] = fcode
            send_code_email.delay(context)
            # print(fcode)
            return JsonResponse({"success":1,"user_inst":user_inst.id,"file_id":file_id},safe=False)

        # print("hi")
        file_id = request.POST.get("file_id")
        user_id = request.POST.get("user_id")
        code = request.POST.get("code")
        # print(code,request.session['f_code'])
        if int(code) == int(request.session['f_code']):
            file_inst = other_books.objects.get(id=file_id)
            user_inst = user_other_book_details.objects.get(id=user_id)
            user_inst.file_downloaded = file_inst
            user_inst.save()
            return JsonResponse({"success":1,"file_url":file_inst.file_link.url,"file_name":file_inst.file_name},safe=False)
        return JsonResponse({"success":0},safe=False)



def contact(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }

    if request.method=="POST":
        name = request.POST.get("name")
        email = request.POST.get("email")
        subject = request.POST.get("subject")
        message = request.POST.get("message")
        add_number = int(request.POST.get("add_number"))
        if add_number==5:
            mail_data = {
                "name":name,
                "email":email,
                "subject":subject,
                "message":message
            }

            send_contact_form_email.delay(mail_data)

            context['sent'] = True
        else:
            context['sent'] = False

        return render(request,'contact.html',context)

    return render(request,'contact.html',context)

def products(request,category):
    cart_inst = get_cart_instance(request)
    if category=="books":
        prod_list_inst = product_table.objects.filter(is_book=True)
    if category=="ebooks":
        prod_list_inst = product_table.objects.filter(is_ebook=True)
    if category=="others":
        prod_list_inst = product_table.objects.filter(is_other_item=True);
    if category=="test-series":
        prod_list_inst = product_table.objects.filter(is_test_series=True);

    cart_items_list=[]
    if cart_inst:
        cart_items_list=cart_inst.cart_items_set.values_list('item', flat=True)
    context = {
        "cart_inst":cart_inst,
        "prod_list_inst":prod_list_inst,
        "cart_items_list":cart_items_list
    }
    return render(request,'products-list.html',context)

def single_product(request,id,slug):
    cart_inst = get_cart_instance(request)
    prod_inst = product_table.objects.get(id=id)
    cart_items_list=[]
    if cart_inst:
        cart_items_list=cart_inst.cart_items_set.values_list('item', flat=True)
    context = {
        "cart_inst":cart_inst,
        "prod_inst":prod_inst,
        "cart_items_list":cart_items_list
    }
    return render(request,'product.html',context)


def add_to_cart(request):
    cart_inst = get_cart_instance(request)

    if request.method=="POST":
        item_id = request.POST.get('item_id')
        item_quantity = int(request.POST.get('item_quantity'))
        prod_inst = product_table.objects.get(id=item_id)

        if not cart_inst:
            cart_inst = cart_table.objects.create(user=None)
        else:
            if prod_inst.is_ebook:
                check_others = cart_inst.cart_items_set.filter(Q(item__is_book=True) | Q(item__is_other_item=True) | Q(item__is_test_series=True)).count()
                if check_others > 0:
                    context = {
                        "cart_inst":cart_inst,
                        "error": "e-books cannot be clubbed with other items"
                    }
                    return render(request,'index.html',context)

            elif prod_inst.is_test_series:
                check_others = cart_inst.cart_items_set.filter(Q(item__is_book=True) | Q(item__is_other_item=True) | Q(item__is_ebook=True)).count()
                if check_others > 0:
                    context = {
                        "cart_inst":cart_inst,
                        "error": "Test Series cannot be clubbed with other items"
                    }
                    return render(request,'index.html',context)

            elif prod_inst.is_book or prod_inst.is_other_item:
                check_others = cart_inst.cart_items_set.filter(Q(item__is_test_series=True) | Q(item__is_ebook=True)).count()
                if check_others > 0:
                    context = {
                        "cart_inst":cart_inst,
                        "error": "e-books/Test Series cannot be clubbed with other items"
                    }
                    return render(request,'index.html',context)

        cart_items_inst, _cart_item = cart_items.objects.get_or_create(
                                            cart = cart_inst,
                                            item = prod_inst
                                            )
        if _cart_item:
            cart_items_inst = cart_items.objects.get(
                                                cart = cart_inst,
                                                item = prod_inst
                                                )
            cart_items_inst.quantity = item_quantity
            # print(item_quantity)
            cart_items_inst.line_total = item_quantity*prod_inst.price
            cart_items_inst.save()

            cart_inst.is_empty = False
            cart_inst.total_price += item_quantity*prod_inst.price

            if  prod_inst.is_test_series or  prod_inst.is_ebook:
                cart_inst.grand_total_price = cart_inst.total_price + 0
                cart_inst.shipping_price = 0
            else:
                if prod_inst.name == "Path to Nobel":
                    cart_inst.grand_total_price = cart_inst.total_price + decimal.Decimal(0.01*item_quantity)
                    cart_inst.shipping_price = decimal.Decimal(0.01*item_quantity)
                else:
                    cart_inst.grand_total_price = cart_inst.total_price + shipping_price
                    cart_inst.shipping_price = shipping_price
            cart_inst.save()


        request.session['cart_id'] = cart_inst.id

    return redirect("cart")


def remove_from_cart(request,item_id):
    cart_inst = get_cart_instance(request)
    prod_inst = product_table.objects.get(id=item_id)
    cart_items_inst = cart_items.objects.get(
                                        cart = cart_inst,
                                        item = prod_inst
                                        )
    # if cart_inst.total_price > 0:
    cart_inst.total_price -= cart_items_inst.line_total

    if cart_inst.total_price == 0:
        cart_inst.is_empty = True
        cart_inst.grand_total_price = 0
    else:
        if prod_inst.name == "Path to Nobel":
            cart_inst.grand_total_price = cart_inst.total_price + decimal.Decimal(0.01*cart_items_inst.quantity)
        else:
            cart_inst.grand_total_price = cart_inst.total_price + shipping_price

    cart_inst.save()

    cart_item_inst = cart_items.objects.get(cart=cart_inst,item=prod_inst)
    cart_item_inst.delete()

    return redirect("cart")


def update_cart(request):
    cart_inst = get_cart_instance(request)
    if request.method=="POST":
        item_id = request.POST.get('item_id')
        item_quantity = request.POST.get('item_quantity',None)
        prod_inst = product_table.objects.get(id=item_id)

        if not item_quantity:
            return redirect('remove-from-cart', item_id=item_id)

        item_quantity = int(item_quantity)

        if item_quantity < 1:
            return redirect('remove-from-cart', item_id=item_id)

        cart_item_inst = cart_items.objects.get(
                                                cart = cart_inst,
                                                item = prod_inst
                                            )

        cart_item_inst.quantity = item_quantity
        cart_item_inst.line_total = item_quantity*prod_inst.price
        cart_item_inst.save()
        cart_all_items = cart_inst.cart_items_set.all()
        cart_inst.total_price = 0
        for i in cart_all_items:
            cart_inst.total_price += i.line_total

        if prod_inst.name == "Path to Nobel":
            cart_inst.grand_total_price = cart_inst.total_price + decimal.Decimal(0.01*item_quantity)
            cart_inst.shipping_price = decimal.Decimal(0.01*item_quantity)
        else:
            cart_inst.grand_total_price = cart_inst.total_price + shipping_price


        cart_inst.save()

    return redirect("cart")


def cart(request):
    cart_inst = get_cart_instance(request)
    ebook_cart = False
    test_series_cart = False
    if cart_inst:
        test_series_chk = cart_inst.cart_items_set.filter(Q(item__is_test_series=True)).count()
        ebook_chk = cart_inst.cart_items_set.filter(Q(item__is_ebook=True)).count()
        if test_series_chk > 0:
            test_series_cart = True
        if ebook_chk >0:
            ebook_cart = True

    context = {
        "cart_inst":cart_inst,
        "test_series_cart":test_series_cart,
        "ebook_cart":ebook_cart
    }
    return render(request,'cart.html',context)


def create_checkout_session(request):
    if request.method == "POST":
        cart_inst = get_cart_instance(request)

        try:
            #Billing information
            name            = request.POST.get('billing_name')
            email           = request.POST.get('billing_email')
            phone_number    = request.POST.get('billing_phone_number')
            street_address  = request.POST.get('billing_street_address')
            city            = request.POST.get('billing_city')
            state           = request.POST.get('billing_state')
            pincode         = request.POST.get('billing_pincode')
            #Shipping information
            ship_user_name            = request.POST.get('name')
            ship_user_email           = request.POST.get('email')
            ship_user_phone_number    = request.POST.get('phone_number')
            ship_user_street_address  = request.POST.get('street_address')
            ship_user_city            = request.POST.get('city')
            ship_user_state            = request.POST.get('state')
            ship_user_pincode         = request.POST.get('pincode')

            cart_id                  = request.POST.get('cart_id')
            is_ship_bill_same         = request.POST.get('is_ship_bill_same')

            random_no       = randrange(1000, 10000)

            try:
                if is_ship_bill_same:
                    ship_user_pincode = pincode
                    ship_user_name  = name
                    ship_user_email = email
                    ship_user_phone_number = phone_number
                    ship_user_street_address = street_address
                    ship_user_city = city
                    ship_user_state = state

                user_inst, user_created = user_information.objects.get_or_create(
                                                                name=ship_user_name,
                                                                email=ship_user_email,
                                                                phone = ship_user_phone_number,
                                                                )

                billing_user_inst, billing_user_created = user_information.objects.get_or_create(
                                                                name=name,
                                                                email=email,
                                                                phone = phone_number,
                                                                )

                shipping_inst, shipping_created = shipping_information.objects.get_or_create(
                                                                user = user_inst,
                                                                street_address = ship_user_street_address,
                                                                pincode = ship_user_pincode,
                                                                city = ship_user_city,
                                                                state = ship_user_state
                                                                )
                billing_inst, billing_created = billing_information.objects.get_or_create(
                                                                user = billing_user_inst,
                                                                street_address = street_address,
                                                                pincode = pincode,
                                                                city = city,
                                                                state = state
                                                                )

                cart_inst.user = user_inst
                cart_inst.save()

                unique_no = email.split("@")[0] + "_" +str(random_no)

            except Exception as e:
                print(e)
                return redirect("cart")

            checkout_session = stripe.checkout.Session.create(
                customer_email = email,
                line_items=[
                    {
                        # TODO: replace this with the `price` of the product you want to sell
                            'name': str(unique_no),
                            'quantity': 1,
                            'currency': 'usd',
                            'amount': str(int(cart_inst.grand_total_price)*100),
                        # 'price':'1'
                    },
                ],
                payment_method_types=[
                  'card',
                ],
                mode='payment',
                success_url = domain + '/success/'+"{CHECKOUT_SESSION_ID}",
                cancel_url = domain + '/cancel/'+str(cart_id),
            )
            try:
                order_table.objects.create(
                                    user=user_inst,
                                    cart = cart_inst,
                                    shipping_address = shipping_inst,
                                    billing_information = billing_inst,
                                    order_id = unique_no,
                                    stripe_payment_intent=checkout_session['payment_intent']
                                )
            except Exception as e:
                print("431",e)
                ord_inst = order_table.objects.get(
                                    cart = cart_inst
                                    )

                ord_inst.shipping_information = shipping_inst
                ord_inst.billing_information = billing_inst
                ord_inst.user = user_inst
                ord_inst.order_id = unique_no
                ord_inst.stripe_payment_intent = checkout_session['payment_intent']
                ord_inst.save()
            # print(checkout_session)
        except Exception as e:
            # return str(e
            print(e)
            context = {
                "cart_inst":cart_inst,
                "error":"payment not created"
            }
            return render(request,'cancel.html',context)

        return redirect(checkout_session.url, code=303)


def success(request,stripe_id):

    session = stripe.checkout.Session.retrieve(stripe_id)

    order_table_inst = get_object_or_404( order_table,
                            stripe_payment_intent = session.payment_intent,
                        )

    order_table_inst.has_paid = True
    order_table_inst.order_date = datetime.now()

    order_table_inst.save()

    cart_inst = cart_table.objects.get(id=order_table_inst.cart.id)
    cart_inst.checkout = True
    cart_inst.save()
    ebook_cart = False
    test_series_cart = False
    # cart_items = cart_inst.cart_items_set.all()

    if cart_inst.cart_items_set.filter(Q(item__is_test_series=True)).count() > 0:
         test_series_cart = True
         # test_code       = randrange(100000, 999999)
         # test_series_code.objects.create(order=order_table_inst,test_code=test_code)
    if cart_inst.cart_items_set.filter(Q(item__is_ebook=True)).count() > 0:
        ebook_cart = True

    if ebook_cart or test_series_cart:
        order_table_inst.delivered=True
        order_table_inst.save()

    context = {
        "order_id":order_table_inst.id,
        "email":cart_inst.user.email,
        "ebook_cart":ebook_cart,
        "test_series_cart":test_series_cart
    }

    send_confirm_email.delay(context)

    context["order_inst"] = order_table_inst
    request.session['cart_id'] = None
    return render(request,'success.html',context)


def cancel(request):
    cart_inst = get_cart_instance(request)

    context = {
        "cart_inst":cart_inst
    }
    return render(request,'cancel.html',context)


def get_my_ebooks(request,order_id):
    order_inst = order_table.objects.get(order_id=str(order_id))
    cart_inst = cart_table.objects.get(id=order_inst.cart.id)
    ebook_list = []
    ebook_list = cart_inst.cart_items_set.all().filter(item__is_ebook = True)
    context = {
        "ebook_list":ebook_list
    }
    return render(request,'my-ebooks.html',context)


def get_my_testSeries(request,order_id):
    order_inst = order_table.objects.get(order_id=str(order_id))
    cart_inst = cart_table.objects.get(id=order_inst.cart.id)
    test_series_list = []
    test_series_list = cart_inst.cart_items_set.filter(item__is_test_series = True)
    context = {
        "test_series_list":test_series_list
    }
    return render(request,'my-test-series.html',context)

def get_code_testSeries(request):
    # if request.method=="POST":
    tcode = '123456'
    test_series_list = []
    test_series_list = test_series_code.objects.filter(test_code=tcode)
    context = {
        "test_inst":test_series_list
    }
    return render(request,'code-test-series.html',context)
    # return render(request,'code-test-series.html')

def nutrition_game(request):
    # if request.method=="POST":

    return render(request,'nutrition-game.html')
    # return render(request,'code-test-series.html')

def extra_book_view(request):
    cart_inst = get_cart_instance(request)
    other_books_inst = extra_books.objects.all()
    context = {
        "cart_inst":cart_inst,
        "other_books_inst":other_books_inst
    }
    return render(request,'extra-book-download.html',context)


def extra_books_user(request):
    if request.method == "POST":
        # print("check")
        if not request.POST.get("code"):
            first_name = request.POST.get("first_name")
            last_name = request.POST.get("last_name")
            email = request.POST.get("email")
            phone_number = request.POST.get("phone_number")
            street_address = request.POST.get("street_address")
            city = request.POST.get("city")
            state = request.POST.get("state")
            pincode = request.POST.get("pincode")
            country = request.POST.get("country")
            comments = request.POST.get("comments")
            file_id = request.POST.get("file_id")

            # file_inst = extra_books.objects.get(id=file_id)
            user_inst, created = extra_book_details.objects.get_or_create(
                                            first_name=first_name,
                                            last_name=last_name,
                                            phone_number=phone_number,
                                            email=email,
                                            street_address=street_address,
                                            city=city,
                                            state=state,
                                            pincode=pincode,
                                            country=country,
                                            comments=comments
                                            # file_downloaded=file_inst
                                        )
            fcode = randint(1000,9999)
            context = {
                "code":fcode,
                "email":email,
                "first_name":first_name
            }
            request.session['f_code'] = fcode
            send_code_email.delay(context)
            # print(fcode)
            return JsonResponse({"success":1,"user_inst":user_inst.id,"file_id":file_id},safe=False)

        # print("hi")
        file_id = request.POST.get("file_id")
        user_id = request.POST.get("user_id")
        code = request.POST.get("code")
        # print(code,request.session['f_code'])
        if int(code) == int(request.session['f_code']):
            file_inst = extra_books.objects.get(id=file_id)
            user_inst = extra_book_details.objects.get(id=user_id)
            user_inst.file_downloaded = file_inst
            user_inst.save()
            return JsonResponse({"success":1,"file_url":file_inst.file_link.url,"file_name":file_inst.file_name},safe=False)
        return JsonResponse({"success":0},safe=False)


# 365 Quinoa Recipe Views
#
# def login(request):
#     if request.method == 'POST':
#         _username   = request.POST.get('username')
#         _password = request.POST.get('password')
#         add_number = int(request.POST.get("add_number"))
#         if add_number==5:
#             user = authenticate(username=_username, password=_password)
#             print(user)
#             if user is not None:
#                 if user.is_active:
#                     print('logged in')
#                     auth_login(request, user)
#                     # return HttpResponseRedirect("project/365-quinoa-recipe/recipe")
#                     return redirect("quinoa-recipes")
#
#                 else:
#                     return render(request,'365project/login.html',{"success":False})
#             else:
#                 return render(request,'365project/login.html',{"success":False})
#     return render(request,'365project/login.html')
#
#
# @login_required(login_url = '/project/login')
# def logout(request):
#     auth_logout(request)
#     return render(request,'365project/login.html')
#
#
# def register(request):
#     if request.method=="POST":
#         username = request.POST.get('username')
#         password = request.POST.get('password')
#         email = request.POST.get('email')
#         add_number = int(request.POST.get("add_number"))
#         if add_number==5:
#             if User.objects.filter(Q(username=username)|Q(email=email)).exists():
#                 messages.warning(request, 'User already exist with same username or email')
#                 return render(request,'365project/register.html')
#             else:
#                 user = User.objects.create_user(username=username, password=password, email=email)
#                 user = authenticate(username=username, password=password)
#                 auth_login(request, user)
#                 messages.success(request, 'Registered successfully!')
#                 return redirect("quinoa-recipe-profile")
#             # return redirect('users:view')
#     return render(request,'365project/register.html')


def quinoa_recipes_book(request):

    return render(request,'365-quinoa-recipe.html')


def path_to_nobel(request):

    return render(request,'path-to-nobel.html')


def path_to_nobel_book(request):

    return render(request,'path-to-nobel-book.html')


def path_to_nobel_book(request):

    return render(request,'path-to-nobel-book.html')


def path_to_nobel_book_2(request):

    return render(request,'path-to-nobel-book-2.html')


def rishis_as_scientist(request):

    return render(request,'rishi-as-scientist.html')


def cues_app_project(request):

    return render(request,'108-cues-app.html')


def handler404(request,exception):
    return render(request, 'page-404.html', status=404)

def handler500(request):
    return render(request, 'page-500.html', status=500)
