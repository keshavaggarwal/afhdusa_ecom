"""afhdecom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from django.conf import settings

from ecommerce.views import (
                                index,
                                path_to_nobel_book_2,
                                products,
                                single_product,
                                cart,
                                add_to_cart,
                                remove_from_cart,
                                update_cart,
                                temples_list,

                                create_checkout_session,
                                contact,
                                success,
                                cancel,
                                get_my_ebooks,
                                get_my_testSeries,
                                get_code_testSeries,
                                handler404,
                                handler500,
                                who_we_are,
                                what_we_do,
                                vision_mission,
                                our_team,
                                project_knowledge,
                                project_nutrition,
                                nutrition_food_file,
                                nutritional_file_user,
                                other_books_download_file,
                                other_free_books_user,
                                extra_book_view,
                                extra_books_user,
                                nutrition_game,

                                # 365-quinoa-recipe
                                quinoa_recipes_book,
                                path_to_nobel,
                                path_to_nobel_book,
                                rishis_as_scientist,
                                cues_app_project

                            )


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index),

    path('ckeditor/',include('ckeditor_uploader.urls')),

    path('project/365-quinoa-recipes-book/', quinoa_recipes_book, name="quinoa-recipes-book"),
    path('project/path-to-nobel/', path_to_nobel, name="path-to-nobel"),
    path('project/path-to-nobel-book-1st-edition/', path_to_nobel_book, name="path-to-nobel-book"),
    path('project/path-to-nobel-book-2nd-edition/', path_to_nobel_book_2, name="path-to-nobel-book-2"),
    path('project/rishis-as-scientist/', rishis_as_scientist, name="rishis-as-scientist"),
    path('project/108-cues-app-project/', cues_app_project, name="108-cues-app-project"),

    # path('about/who-we-are/', who_we_are, name="who-we-are"),

    path('contact/', contact, name="contact"),    

    path('dashboard/', include(("dashboard.urls",'dashboard'), namespace='dashboard')),


    path('about/who-we-are/', who_we_are, name="who-we-are"),
    path('about/what-we-do/', what_we_do, name="what-we-do"),
    path('about/vision-mission/', vision_mission, name="vision-mission"),
    path('about/our-team/', our_team, name="our-team"),
    path('project/nutrition/', project_nutrition, name="project-nutrition"),
    path('project/knowledge/', project_knowledge, name="project-knowledge"),


    path('items/<category>', products, name="buy-items"),
    path('item/<id>/<slug>', single_product,name="single-item"),

    path('remove-from-cart/<item_id>', remove_from_cart, name="remove-from-cart"),
    path('add-to-cart/', add_to_cart, name="add-to-cart"),
    path('cart/', cart, name="cart"),
    path('update-cart/', update_cart, name="update-cart"),

    path('temples-list/', temples_list, name="temples_list"),


    path('nutrition-food-file/', nutrition_food_file, name="nutrition-food-file"),
    path('nutritional-file-user/', nutritional_file_user, name="nutritional-file-user"),

    path('download-book/', other_books_download_file, name="download-books"),
    path('download-book-user/', other_free_books_user, name="download-book-user"),


    path('at-the-feet-of-mother-india/', extra_book_view, name="at-the-feet-of-mother-india"),
    path('extra-book-user/', extra_books_user, name="extra-book-user"),


    path('checkout', create_checkout_session,name='checkout'),

    path('success/<stripe_id>', success),
    path('cancel/<cart_id>', cancel,name="cancel"),

    path('ebooks/<order_id>', get_my_ebooks,name='my-ebooks'),
    path('test-series/<order_id>', get_my_testSeries,name='my-test-series'),

    path('code-test-series/', get_code_testSeries,name='code-test-series'),

    path('nutrition-game/', nutrition_game,name='nutrition-game'),

    path('_nested_admin/', include('nested_admin.urls')),


]


handler404 = handler404
handler500 = handler500


if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
