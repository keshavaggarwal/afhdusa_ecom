"""afhdecom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static

from django.conf import settings

from dashboard.views import (
                                analytics,
                                users,
                                orders,
                                dnd_orders,
                                cart_detail_view,
                                view_products,
                                add_products,
                                book_download_users,
                                login,
                                logout
                            )


urlpatterns = [

    path('', analytics, name="analytics"),
    path('login/', login, name="login"),
    path('logout/', logout, name="logout"),

    path('users/', users, name="users"),
    path('book-download-users/', book_download_users, name="book-download-users"),

    path('order/<status>', orders, name="orders"),
    path('update/order/', dnd_orders, name="order-status"),
    path('details/order-cart/', cart_detail_view, name="cart-detail"),


    path('list/products/', view_products, name="products"),
    path('add/products/', add_products, name="add-products")

]
