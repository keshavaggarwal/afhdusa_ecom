from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from django.contrib.auth import authenticate, login as auth_login,logout as auth_logout
from django.contrib.auth.decorators import login_required


from ecommerce.models import (   user_information,
                        product_category,
                        product_table,
                        cart_table,
                        cart_items,
                        shipping_information,
                        user_other_book_details,
                        order_table
                    )


# Create your views here.

def login(request):
    if request.method == 'POST':
        _username   = request.POST.get('username')
        _password = request.POST.get('password')
        user = authenticate(username=_username, password=_password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return HttpResponseRedirect("/dashboard")
            else:
                return render(request,'dashboard/login.html',{"success":False})
        else:
            return render(request,'dashboard/login.html',{"success":False})
    return render(request,'dashboard/login.html')


@login_required(login_url = 'dashboard:login')
def logout(request):
    auth_logout(request)
    return render(request,'dashboard/login.html')


@login_required(login_url = 'dashboard:login')
def analytics(request):
    order_inst = order_table.objects.filter(has_paid=True)
    total_amount = 0
    user_inst = user_information.objects.all()
    prod_inst = product_table.objects.all()

    for i in order_inst:
        total_amount += i.cart.grand_total_price
    context={
        "total_orders":order_inst.count(),
        "total_order_amount":total_amount,
        "user_inst":user_inst.count(),
        "prod_inst":prod_inst.count()
    }
    return render(request,'dashboard/analytics.html',context)


@login_required(login_url = 'dashboard:login')
def users(request):
    user_inst = user_information.objects.all()
    context={
        "user_inst":user_inst
    }
    return render(request,'dashboard/user-list.html',context)


@login_required(login_url = 'dashboard:login')
def book_download_users(request):
    user_inst = user_other_book_details.objects.all().order_by('-timestamp')
    context={
        "user_inst":user_inst
    }
    return render(request,'dashboard/book-download-user-list.html',context)


@login_required(login_url = 'dashboard:login')
def orders(request,status):
    if status=="delivered":
        order_inst = order_table.objects.filter(delivered=True).order_by("-order_date")
    else:
        order_inst = order_table.objects.filter(delivered=False).order_by("-order_date")
    context={
        "order_inst":order_inst
    }
    return render(request,'dashboard/order-list.html',context)


@login_required(login_url = 'dashboard:login')
def view_products(request):
    prod_inst = product_table.objects.all()
    context={
        "prod_inst":prod_inst
    }
    return render(request,'dashboard/product-list.html',context)


@login_required(login_url = 'dashboard:login')
def add_products(request):

    return render(request,'dashboard/product-add.html',context)


@login_required(login_url = 'dashboard:login')
def cart_detail_view(request):
    cart_id = request.POST.get('cart_id')
    cart_inst = cart_table.objects.get(id=cart_id)
    items = cart_inst.cart_items_set.all()
    ship_obj = order_table.objects.get(cart=cart_inst)
    cart_list={}
    items_list=[]
    for e,j in enumerate(items):
        if j.item.is_book:
            items_list.append({"name":j.item.name,"price":j.item.price,"type":"Book","quantity":j.quantity,"line_total":j.line_total})
        if j.item.is_ebook:
            items_list.append({"name":j.item.name,"price":j.item.price,"type":"E-book","quantity":j.quantity,"line_total":j.line_total})
        if j.item.is_other_item:
            items_list.append({"name":j.item.name,"price":j.item.price,"type":"Others","quantity":j.quantity,"line_total":j.line_total})

    cart_list['cart_det'] = items_list
    cart_list['total'] = cart_inst.grand_total_price
    cart_list['shipping_charges'] = cart_inst.shipping_price

    cart_list['address'] = {"street":ship_obj.shipping_address.street_address,"city":ship_obj.shipping_address.city,"state":ship_obj.shipping_address.state,"pincode":ship_obj.shipping_address.pincode}

    return JsonResponse(cart_list,safe=False)


@login_required(login_url = 'dashboard:login')
def dnd_orders(request):
    id = request.POST.get('id')
    status = request.POST.get('status')
    if status == "delivered":
        order_table.objects.filter(id=id).update(delivered=False)
        return JsonResponse({"result":"Deactivated"})
    else:
        order_table.objects.filter(id=id).update(delivered=True)
        return JsonResponse({"result":"Activated"})
